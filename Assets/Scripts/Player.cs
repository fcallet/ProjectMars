﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public Transform shipTransform;
    public Transform rotationPoint;
    public Rigidbody rigidbody;

    public Transform HologramShip;
    public Transform rotationPointHologram;

    public float RotationSpeedYawMultiplier { get; set; }
    public float RotationSpeedPitchMultiplier { get; set; }
    public float TranslationSpeedMutliplier { get; set; }

    private float maxSpeedRotation = 50f;
    private float maxRotationZ = 90;
    private float maxSpeedTranslation = 1300;

    bool canMakePitchRotationUp;
    bool canMakePitchRotationDown;

    //Oxygen and Water
    public GameObject OxygenNotif;
    public GameObject WaterNotif;
    private const int MAX_OXYGEN = 300;
    private const int MAX_WATER = 100;
    private const float TIME_BEFORE_OXYGEN_LOSS = 1;
    private const float TIME_BEFORE_WATER_LOSS = 3;
    private int Oxygen;
    private int Water;
    private bool inOxygenZone;
    private bool inWaterZone;
    private float timeBeforeOxygenLoss;
    private float timeBeforeWaterLoss;
    public HUDManager HUDManager;

    //Inventory
    public InventoryManager inventoryManager;
    private int CopperNumber;
    private int AluminiumNumber;
    private int IronNumber;

	public GameObject gameoverPanel;
	public GameObject menu3D;


    public void SetRotationSpeedYawMultiplier(float speed)
    {
        RotationSpeedYawMultiplier = speed;
    }

    public void SetRotationSpeedPitchMultiplier(float speed)
    {
        RotationSpeedPitchMultiplier = speed;
    }

    public void SetTranslationSpeedMultiplier(float speed)
    {
        TranslationSpeedMutliplier = speed;
    }


    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(transform.parent.gameObject);
        RotationSpeedYawMultiplier = 0;
        RotationSpeedPitchMultiplier = 0;
        TranslationSpeedMutliplier = 0;
        canMakePitchRotationUp = true;
        canMakePitchRotationDown = true;
        inWaterZone = false;
        inOxygenZone = false;
        Oxygen = MAX_OXYGEN;
        Water = MAX_WATER;
        timeBeforeOxygenLoss = TIME_BEFORE_OXYGEN_LOSS;
        timeBeforeWaterLoss = TIME_BEFORE_WATER_LOSS;
        IronNumber = 0;
        CopperNumber = 0;
        AluminiumNumber = 0;
        inventoryManager.NotifyRessourcesNumber(IronNumber, CopperNumber, AluminiumNumber);
        //rigidbody.maxAngularVelocity = maxSpeedRotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		
        CalculateMovement();
        UpdateWaterAndOxygen();
    }

    private void CalculateMovement()
    {
        float newAngle1 = RotationSpeedYawMultiplier * maxSpeedRotation * Time.fixedDeltaTime;
        float newAngle2 = RotationSpeedPitchMultiplier * maxSpeedRotation * Time.fixedDeltaTime;

        shipTransform.RotateAround(rotationPoint.position, new Vector3(0, 1, 0), newAngle1);

        HologramShip.RotateAround(rotationPointHologram.position, transform.parent.up, newAngle1);
        canMakePitchRotationUp = shipTransform.localEulerAngles.x > 301 && shipTransform.localEulerAngles.x <= 380 || shipTransform.localEulerAngles.x >= -0.01f && shipTransform.localEulerAngles.x < 90;
        canMakePitchRotationDown = shipTransform.localEulerAngles.x >= -0.01f && shipTransform.localEulerAngles.x < 89 || shipTransform.localEulerAngles.x > 300 && shipTransform.localEulerAngles.x <= 380;
        if (!canMakePitchRotationUp)
        {
            if (RotationSpeedPitchMultiplier > 0)
            {
                shipTransform.RotateAround(rotationPoint.position, rotationPoint.right, newAngle2);
                HologramShip.RotateAround(rotationPointHologram.position, rotationPointHologram.right, newAngle2);
            }
        }
        else if (!canMakePitchRotationDown)
        {
            if (RotationSpeedPitchMultiplier < 0)
            {
                shipTransform.RotateAround(rotationPoint.position, rotationPoint.right, newAngle2);
                HologramShip.RotateAround(rotationPointHologram.position, rotationPointHologram.right, newAngle2);
            }
        }
        else
        {
            shipTransform.RotateAround(rotationPoint.position, rotationPoint.right, newAngle2);
            HologramShip.RotateAround(rotationPointHologram.position, rotationPointHologram.right, newAngle2);
        }

        rigidbody.AddForce(TranslationSpeedMutliplier * maxSpeedTranslation * transform.parent.forward);

        if (rigidbody.velocity.magnitude > maxSpeedTranslation)
        {
            rigidbody.AddForce(-rigidbody.velocity.normalized * maxSpeedTranslation);
        }
        if (Vector3.Dot(rigidbody.velocity.normalized, transform.parent.forward) < 0)
        {
            rigidbody.AddForce(-rigidbody.velocity.normalized * maxSpeedTranslation);
        }
    }

    private void UpdateWaterAndOxygen()
    {
        timeBeforeOxygenLoss -= Time.fixedDeltaTime;
        timeBeforeWaterLoss -= Time.fixedDeltaTime;

        if(timeBeforeOxygenLoss <= 0)
        {
            if (inOxygenZone)
                Oxygen += 5;
            else
                Oxygen--;
            timeBeforeOxygenLoss = TIME_BEFORE_OXYGEN_LOSS;
        }
        
        if(timeBeforeWaterLoss <= 0)
        {
            if (inWaterZone)
                Water+=5;
            else
                Water--;
            timeBeforeWaterLoss = TIME_BEFORE_WATER_LOSS;
        }

        HUDManager.NotifyRemainingOxygenAndWater(Oxygen * 100 / MAX_OXYGEN, Water * 100 / MAX_WATER);

        if (Oxygen == 0 || Water == 0)
        {
			GetComponent<LeapMotionController>().enabled = false;
			RotationSpeedPitchMultiplier = 0;
			RotationSpeedYawMultiplier = 0;
			TranslationSpeedMutliplier = 0;
			gameoverPanel.SetActive (true);
			menu3D.SetActive (true);
        }

    }

    public void AddCopper(int amount)
    {
        CopperNumber+= amount;
        inventoryManager.UpdateCopperNumber(CopperNumber);
    }

    public void AddIron(int amount)
    {
        IronNumber+= amount;
        inventoryManager.UpdateIronNumber(IronNumber);
    }

    public void AddAluminium(int amount)
    {
        AluminiumNumber+= amount;
        inventoryManager.UpdateAluminiumNumber(AluminiumNumber);
    }

    public void PositionPlayerOnMars()
    {
        shipTransform.position = new Vector3(6931, 5496, 9341);
    }

    public void FreezePlayer()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        TranslationSpeedMutliplier = 0;
        RotationSpeedPitchMultiplier = 0;
        RotationSpeedYawMultiplier = 0;
    }


    public bool CanCreateOxygenHouse()
    {
        bool value = false;
        if(CopperNumber >= 5 && IronNumber >= 3 && AluminiumNumber >= 4)
        {
            CopperNumber -= 5;
            IronNumber -= 3;
            AluminiumNumber -= 4;
            inventoryManager.NotifyRessourcesNumber(IronNumber, CopperNumber, AluminiumNumber);
            value = true;
        }
        return value;
    }

    public bool CanCreateWaterStation()
    {
        bool value = false;
        if (CopperNumber >= 3 && IronNumber >= 2 && AluminiumNumber >= 5)
        {
            CopperNumber -= 3;
            IronNumber -= 2;
            AluminiumNumber -= 5;
            inventoryManager.NotifyRessourcesNumber(IronNumber, CopperNumber, AluminiumNumber);
            value = true;
        }
        return value;
    }

    public void NotifyWaterZoneProximity(bool isInZone)
    {
        this.inWaterZone = isInZone;
        WaterNotif.SetActive(isInZone);
    }

    public void NotifyOxygenZoneProximity(bool isInZone)
    {
        this.inOxygenZone = isInZone;
        OxygenNotif.SetActive(isInZone);
    }


}