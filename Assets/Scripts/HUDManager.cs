﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {

    public Slider OxygenBar;
    public Slider WaterBar;

	// Use this for initialization
	void Start () {
		
	}

    public void NotifyRemainingOxygenAndWater(int percentageOxygen, int percentageWater)
    {
        OxygenBar.value = percentageOxygen;
        WaterBar.value = percentageWater;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
