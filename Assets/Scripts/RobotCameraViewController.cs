﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RobotCameraViewController : MonoBehaviour {

	public RenderTexture robotCameraRenderTexture;
	GameObject robot;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<RawImage> ().texture = robotCameraRenderTexture;
	}

	public void OpenRobotCamera(){

		robot = GameObject.FindGameObjectWithTag ("Robot");
		if (robot != null) {
			GameObject.FindGameObjectWithTag ("Robot").GetComponentInChildren<Camera> ().enabled = true;
			GameObject.FindGameObjectWithTag ("Robot").GetComponentInChildren<Camera> ().targetTexture = robotCameraRenderTexture;
			GetComponent<RawImage> ().enabled = true;
			GetComponent<RawImage> ().texture = robotCameraRenderTexture;
		}
	}

	public void CloseRobotCamera(){
		
		robot = GameObject.FindGameObjectWithTag ("Robot");
		if (robot != null) {
			GetComponent<RawImage> ().enabled = false;
			GameObject.FindGameObjectWithTag ("Robot").GetComponentInChildren<Camera> ().enabled = false;
		}
	}
}
