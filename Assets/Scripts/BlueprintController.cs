﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BlueprintController : MonoBehaviour {

	public Player player;
	public Transform finger;
    public Transform spawnPoint;
	public InventoryManager inventoryManager;
	public GameObject robotPrefab;
	public GameObject greenhousePrefab;
	public GameObject waterstationPrefab;
	float timer = 0.0f;
	int seconds;

    GameObject currentRobot;
    GameObject currentOxygenHouse;
    GameObject currentWaterStation;

    // Use this for initialization
    void Start () {
		
	}
	
	void Update(){
		
		timer += Time.deltaTime;
		seconds = Convert.ToInt32 (timer % 60);
	}

	public void OnLeftHandBeginFacingCamera(){
		
		player.GetComponent<LeapMotionController>().enabled = false;
		player.RotationSpeedPitchMultiplier = 0;
		player.RotationSpeedPitchMultiplier = 0;
		player.TranslationSpeedMutliplier = 0;
        inventoryManager.Show();
	}

	public void OnLeftHandEndFacingCamera(){
		player.GetComponent<LeapMotionController> ().enabled = true;
        inventoryManager.Hide();
	}

	public void CreateRobot(){
		if (seconds >= 2 && currentRobot == null) {
			currentRobot = Instantiate (robotPrefab, spawnPoint.position , Quaternion.identity) as GameObject;
            currentRobot.GetComponentInChildren<Robot>().SetPlayer(player);
			timer = 0.0f;
		}

		//robotPrefab.GetComponent<Camera>().ren
	}

	public void CreateGreenhouse(){
		if (seconds >= 2 && currentOxygenHouse == null) {
            if (player.CanCreateOxygenHouse())
            {
                currentOxygenHouse = Instantiate(greenhousePrefab, spawnPoint.position, greenhousePrefab.transform.rotation) as GameObject;
                currentOxygenHouse.GetComponent<OxygenHouse>().SetPlayer(player);
                timer = 0.0f;
            }
		}
	}

	public void CreateWaterstation(){
		if (seconds >= 2 && currentWaterStation == null) {
            if (player.CanCreateWaterStation())
            {
                currentWaterStation = Instantiate(waterstationPrefab, spawnPoint.position, Quaternion.identity) as GameObject;
                currentWaterStation.GetComponent<WaterMachine>().SetPlayer(player);
                timer = 0.0f;
            }
		}
	}
}
