﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;

public class LeapMotionController : MonoBehaviour {

    public Player player;
	public MenuController menuController;

    private Controller controller;
    private Hand leftHand;
    private Vector leftOriginPosition;
    private bool firstDetectionLeft;
    private const float HandSensitivityYaw = 0.05f;
    private const float HandSensitivityPitch = 0.025f;

    private Hand rightHand;
    private Vector rightOriginPosition;
    private bool firstDetectionRight;
    private const float HandSensitivitySpeed = 0.04f;


    // Use this for initialization
    void Start () {
        firstDetectionLeft = false;
        firstDetectionRight = false;
        controller = new Controller();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        GetHands();
        if (leftHand != null)
        {
            ComputeLeftHandPosition();
        }

        if (rightHand != null)
        {
            ComputeRightHandPosition();
			DetectRightThumbExtended ();
			DetectFingersToCloseRobotWindow ();

        }

    }

    private void GetHands()
    {
       
        leftHand = null; rightHand = null;
        Frame frame = controller.Frame();
        List<Hand> hands = frame.Hands;
        for (int i = 0; i < hands.Count; i++)
        {
            if (hands[i].IsLeft)
                leftHand = hands[i];
            if (hands[i].IsRight)
                rightHand = hands[i];
        }
    }

    private void ComputeLeftHandPosition()
    {
        List<Finger> fingers = leftHand.Fingers;
        bool hasAnExtendedFinger = false;
        bool allFingerExtended = true;
        foreach (Finger finger in fingers)
        {
            hasAnExtendedFinger |= finger.IsExtended;
            allFingerExtended &= finger.IsExtended;
        }
        if (!hasAnExtendedFinger)
        {
            if (!firstDetectionLeft)
            {
                firstDetectionLeft = true;
                leftOriginPosition = leftHand.PalmPosition;
            }
            else
            {
                player.RotationSpeedYawMultiplier = (leftHand.PalmPosition.x - leftOriginPosition.x) * HandSensitivityYaw;
                player.RotationSpeedPitchMultiplier = -(leftHand.PalmPosition.y - leftOriginPosition.y) * HandSensitivityPitch;
                if (player.RotationSpeedYawMultiplier > 1)
                    player.RotationSpeedYawMultiplier = 1;
                else if (player.RotationSpeedYawMultiplier < -1)
                    player.RotationSpeedYawMultiplier = -1;

                if (player.RotationSpeedPitchMultiplier > 1)
                    player.RotationSpeedPitchMultiplier = 1;
                else if (player.RotationSpeedPitchMultiplier < -1)
                    player.RotationSpeedPitchMultiplier = -1;
            }
        }
        else
        {
            firstDetectionLeft = false;
            player.RotationSpeedYawMultiplier = 0;
            player.RotationSpeedPitchMultiplier = 0;
        }
    }

    private void ComputeRightHandPosition()
    {
        List<Finger> fingers = rightHand.Fingers;
        bool hasAnExtendedFinger = false;
        foreach (Finger finger in fingers)
        {
            hasAnExtendedFinger |= finger.IsExtended;
        }
        if (!hasAnExtendedFinger)
        {
            if (!firstDetectionRight)
            {
                firstDetectionRight = true;
                rightOriginPosition = rightHand.PalmPosition;
            }
            else
            {
                player.TranslationSpeedMutliplier = -(rightHand.PalmPosition.z - rightOriginPosition.z) * HandSensitivitySpeed;

                if (player.TranslationSpeedMutliplier > 1)
                    player.TranslationSpeedMutliplier = 1;
                else if (player.TranslationSpeedMutliplier < -0.2f)
                    player.TranslationSpeedMutliplier = -0.2f;
            }
        }
        else
        {
            //firstDetectionRight = false;
            //translationSpeedMutliplier = 0;
        }
    }

	// detect if the thumb of right hand is extended to show the robot camera 
	public void DetectRightThumbExtended(){

		List<Finger> fingers = rightHand.Fingers;
		bool rightThumbExtended = false;
		bool otherFingersExtended = false;
		foreach (Finger finger in fingers)
		{
			if (finger.Type == Finger.FingerType.TYPE_THUMB) {
				if (finger.IsExtended) {
					rightThumbExtended = true;
				}
			} else {
				if (finger.IsExtended) {
					otherFingersExtended = true;
				}
			}
		}

		if (rightThumbExtended && !otherFingersExtended) {
			menuController.ShowRobotCameraView ();
		}
	}

	// detect if all fingers of right hand are exteded to hide the robot camera
	public void DetectFingersToCloseRobotWindow(){

		List<Finger> fingers = rightHand.Fingers;
		bool allFingersExtended = true;
		bool allFingersClosed = true;
		foreach (Finger finger in fingers)
		{
			if (!finger.IsExtended) {
				allFingersExtended = false;
			} else {
				allFingersClosed = false;
			}
		}

		if (allFingersExtended || allFingersClosed) {
			menuController.HideRobotCameraView ();
		}
	}

    private void OnLevelWasLoaded(int level)
    {
        enabled = true;
    }
}
