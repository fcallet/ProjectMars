﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TransitionType { Space, Mars };

public class LoadingScreenManager : MonoBehaviour
{

    public Image loadingScreenBackgroundImage;
    public Text loadingScreenText;

    public Sprite SpaceBackground;
    public Sprite MarsBackground;

    private const float animationDuration = 0.2f;
    private float currentTime;

    bool showAnimation;
    bool hideAnimation;

    // Use this for initialization
    void Start()
    {
        showAnimation = false;
        hideAnimation = false;
        currentTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(showAnimation && hideAnimation)
        {
            currentTime += Time.deltaTime;
            loadingScreenBackgroundImage.color = new Color(loadingScreenBackgroundImage.color.r, loadingScreenBackgroundImage.color.g, loadingScreenBackgroundImage.color.b, (currentTime / animationDuration));
            loadingScreenText.color = new Color(loadingScreenText.color.r, loadingScreenText.color.g, loadingScreenText.color.b, (currentTime / animationDuration));
        }
        else if(showAnimation)
        {
            currentTime += Time.deltaTime;
            loadingScreenBackgroundImage.color = new Color(loadingScreenBackgroundImage.color.r, loadingScreenBackgroundImage.color.g, loadingScreenBackgroundImage.color.b, (currentTime / animationDuration));
            loadingScreenText.color = new Color(loadingScreenText.color.r, loadingScreenText.color.g, loadingScreenText.color.b, (currentTime / animationDuration));
        }
        else if(hideAnimation)
        {
            currentTime += Time.deltaTime;
            loadingScreenBackgroundImage.color = new Color(loadingScreenBackgroundImage.color.r, loadingScreenBackgroundImage.color.g, loadingScreenBackgroundImage.color.b, 1 -(currentTime / animationDuration));
            loadingScreenText.color = new Color(loadingScreenText.color.r, loadingScreenText.color.g, loadingScreenText.color.b, 1 - (currentTime / animationDuration));
        }

        if (currentTime >= animationDuration)
        {
            currentTime = 0;
            if (showAnimation && hideAnimation)
                showAnimation = false;
            if (showAnimation)
                showAnimation = false;
            else if (hideAnimation)
                hideAnimation = false;
        }
    }

    public void Show(TransitionType transitionType)
    {
        if(transitionType == TransitionType.Mars)
        {
            loadingScreenBackgroundImage.sprite = MarsBackground;
        }
        else if(transitionType == TransitionType.Space)
        {
            loadingScreenBackgroundImage.sprite = SpaceBackground;
        }
        showAnimation = true;
        
    }

    private void Hide()
    {
        hideAnimation = true;
    }

    private void OnLevelWasLoaded(int level)
    {
        Hide();
    }
}
