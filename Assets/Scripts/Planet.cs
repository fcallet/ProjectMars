﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Planet : MonoBehaviour {

    public Transform sun;
    public LoadingScreenManager loadingScreenManager;
    private Transform thisPlanet;

    public float rotationSpeed;
    public Vector3 rotationAxis;
    public bool canEnter;

    private AsyncOperation asyncLoadLevel;

    // Use this for initialization
    void Start () {
        thisPlanet = transform;
	}
	
	// Update is called once per frame
	void Update () {
        thisPlanet.RotateAround(sun.position, rotationAxis, rotationSpeed * Time.deltaTime);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (canEnter&& other.GetComponent<CollisionController>() != null)
        {
#if UNITY_EDITOR
            loadingScreenManager.Show(TransitionType.Mars);
#endif
            Player player = other.GetComponentInChildren<Player>();
            player.GetComponent<LeapMotionController>().enabled = false;
            player.FreezePlayer();
            player.PositionPlayerOnMars();
            StartCoroutine(LoadMarsScene(player));
            

        }
        
    }

    private IEnumerator LoadMarsScene(Player player)
    {
        asyncLoadLevel = SceneManager.LoadSceneAsync("Mars", LoadSceneMode.Single);
        while (!asyncLoadLevel.isDone)
        {
            yield return null;
        }
    }
}
