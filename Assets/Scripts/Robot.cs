﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Robot : MonoBehaviour {

	public GameObject robotHead;
	public GameObject robotBody;
	public bool launched;
	public Rigidbody body;
    //public Robot robot;
    private Player player;

	private Vector3 headPosition;
	private Vector3 bodyPosition;

	void Start () {
		

	}
	

	void Update () {
		if (launched) {
			pickClosestResource (transform.position, 3000f);


			//transform.Translate(new Vector3(Time.deltaTime*Random.Range(-2,8), 0,Time.deltaTime*Random.Range(-5,2)), Space.World );
			robotBody.transform.Rotate (Vector3.up * Time.deltaTime * 400f, Space.World);
			bodyPosition = robotBody.transform.position;
			headPosition = new Vector3 (bodyPosition.x, bodyPosition.y + 0.0034822f, bodyPosition.z);
			robotHead.transform.position = headPosition;


		} else {
			bodyPosition = robotBody.transform.position;
			headPosition = new Vector3 (bodyPosition.x, bodyPosition.y + 0.0034822f, bodyPosition.z);
			robotHead.transform.position = headPosition;
		}
	}

	void OnCollisionEnter (Collision  collision) {

		if (collision.gameObject.tag == "Terrain") {
			launched = true;
		}
		if (collision.gameObject.tag == "Resources") {

            //put resources in the inventory
            //inventory.AddItem (collision.gameObject.GetComponent<Resource> ());
            player.AddIron(5);
            player.AddCopper(10);
            player.AddAluminium(2);
			Destroy (collision.gameObject);
            
			pickClosestResource(transform.position, 3000f);

		}

	}



	void pickClosestResource(Vector3 center, float radius)
	{
		Collider[] hitColliders = Physics.OverlapSphere(center, radius);
		List<GameObject> resources = new List<GameObject>();
		foreach(Collider collider in hitColliders){
			if (collider.gameObject.tag == "Resources") {
				resources.Add (collider.gameObject);
			}
		}

		if (resources.Count > 0) {
			GameObject closestResource = resources [0];
			foreach (GameObject resource in resources) {
				if (Vector3.Distance (transform.position, resource.transform.position) < Vector3.Distance (transform.position, closestResource.transform.position)) {
					closestResource = resource;
				}
			}

			Vector3 direction = closestResource.transform.position - transform.position;
			direction = direction.normalized;
			body.AddForce (direction * 400);
		} else {
			//Destroy (this.transform.parent.gameObject);
		}

	}

    public void SetPlayer(Player player)
    {
        this.player = player;
    }






}
