﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionController : MonoBehaviour {

	public Camera mainCamera;
	public Player player;

    public Rigidbody Rigidbody;
    public CapsuleCollider shipCapsuleCollider;

    public Rigidbody spaceShipPart1;
    public Rigidbody spaceShipPart2;
    public Rigidbody spaceShipPart3;
    public Rigidbody spaceShipPart4;
    public Rigidbody spaceShipPart5;
    public Rigidbody spaceShipPart6;
    public Rigidbody spaceShipPart7;

	public GameObject gameoverPanel;
	public GameObject menu3D;

    AsyncOperation asyncLoadLevel;

    // Use this for initialization
    void Start () {;

    }
	
	// Update is called once per frame
	void Update () {
    }

	private void OnCollisionEnter (Collision  collision)
    {
        player.GetComponent<LeapMotionController>().enabled = false;
        
		if (collision.gameObject.GetComponent<Planet>() == null || !collision.gameObject.GetComponent<Planet>().canEnter)
        {
            
            mainCamera.GetComponent<Animation>().Play();
            player.RotationSpeedPitchMultiplier = 0;
            player.RotationSpeedYawMultiplier = 0;
            player.TranslationSpeedMutliplier = 0;

            Rigidbody.velocity = new Vector3(0, 0, 0);
            Rigidbody.freezeRotation = true;
            shipCapsuleCollider.enabled = false;
            spaceShipPart1.isKinematic = false;
            spaceShipPart1.AddRelativeForce(-collision.impulse);
            spaceShipPart2.isKinematic = false;
            spaceShipPart2.AddRelativeForce(-collision.impulse);
            spaceShipPart3.isKinematic = false;
            spaceShipPart3.AddRelativeForce(-collision.impulse);
            spaceShipPart4.isKinematic = false;
            spaceShipPart4.AddRelativeForce(-collision.impulse);
            spaceShipPart5.isKinematic = false;
            spaceShipPart5.AddRelativeForce(-collision.impulse);
            spaceShipPart6.isKinematic = false;
            spaceShipPart6.AddRelativeForce(-collision.impulse);
            spaceShipPart7.isKinematic = false;
            spaceShipPart7.AddRelativeForce(-collision.impulse);

			gameoverPanel.SetActive (true);
			menu3D.SetActive (true);
        }
        



    }
    

    
}
