﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

	public Player player;
	public GameObject menu3D;
	public RobotCameraViewController robotCameraViewController;
    private AsyncOperation asyncLoad;


    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnRightHandBeginFacingCamera(){
		/*
		player.GetComponent<LeapMotionController>().enabled = false;

		player.RotationSpeedPitchMultiplier = 0;
		player.RotationSpeedYawMultiplier = 0;
		player.TranslationSpeedMutliplier = 0;
		menu3D.SetActive (true); */




	}

	public void OnRightHandEndFacingCamera(){
		/*
		player.GetComponent<LeapMotionController> ().enabled = true;
		menu3D.SetActive (false);*/




	}

	public void ShowRobotCameraView(){
		
		robotCameraViewController.gameObject.SetActive (true);
		robotCameraViewController.OpenRobotCamera ();
	}

	public void HideRobotCameraView(){
		
		robotCameraViewController.CloseRobotCamera ();
		robotCameraViewController.gameObject.SetActive (false);
	}

	public void OnClickRestart()
    {
        Destroy(player.transform.parent.gameObject);
        StartCoroutine(LoadAsyncSpaceScene());
        

    }

    private IEnumerator LoadAsyncSpaceScene()
    {
        // The Application loads the Scene in the background at the same time as the current Scene.
        //This is particularly good for creating loading screens. You could also load the scene by build //number.
        asyncLoad = SceneManager.LoadSceneAsync("Space");

        //Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void OnClickQuit(){
        Application.Quit();
	}


}
