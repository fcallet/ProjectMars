﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {


    
    public Text IronNumberText;
    public Text CopperNumberText;
    public Text AluminiumNumberText;

	// Use this for initialization
	void Start () {

        IronNumberText.text = 0.ToString();
        CopperNumberText.text = 0.ToString();
        AluminiumNumberText.text = 0.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void NotifyRessourcesNumber(int IronNumber, int CopperNumber, int AluminiumNumber)
    {
        IronNumberText.text = IronNumber.ToString();
        CopperNumberText.text = CopperNumber.ToString();
        AluminiumNumberText.text = AluminiumNumber.ToString();
    }

    public void UpdateIronNumber(int IronNumber)
    {
        IronNumberText.text = IronNumber.ToString();
    }

    public void UpdateCopperNumber(int CopperNumber)
    {
        CopperNumberText.text = CopperNumber.ToString();
    }

    public void UpdateAluminiumNumber(int AluminiumNumber)
    {
        AluminiumNumberText.text = AluminiumNumber.ToString();
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
