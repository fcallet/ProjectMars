﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenHouse : MonoBehaviour
{

    bool launched;
    private Player player;
    // Use this for initialization
    void Start()
    {
        launched = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null && launched)
        {
            if (Vector3.Distance(player.transform.position, transform.position) < 10000)
            {
                player.NotifyOxygenZoneProximity(true);
            }
            else
            {
                player.NotifyOxygenZoneProximity(false);
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Terrain")
        {
            launched = true;
        }
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }
}